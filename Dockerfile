FROM registry.access.redhat.com/ubi8/python-39:1-10

COPY . .

RUN pip install --upgrade pip \
    && pip install -r requirements.txt

ENTRYPOINT [ "./helsinki/main.py", "rabbit-mq" ]
